<?php
Route::group([
    'namespace'  => 'KDA\Album\Admin\Http\Controllers\Admin',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware()],
], function () {
    if (config('kda.album-admin.setup_routes', true)) {

            Route::crud('album', 'AlbumCrudController');
    }
});
