<?php

namespace KDA\Album\Admin\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;

/**
 * Class AlbumCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AlbumCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    use \Gaspertrix\LaravelBackpackDropzoneField\App\Http\Controllers\Operations\MediaOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(config('kda.album-admin.model_album'));
        CRUD::setRoute(config('backpack.base.route_prefix') . '/album');
        CRUD::setEntityNameStrings('album', 'albums');
        $this->loadPermissions('album');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('title');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::removeSaveAction('save_and_back');
        CRUD::removeSaveAction('save_and_new');
        CRUD::removeSaveAction('save_and_preview');
        CRUD::field('title');
        CRUD::field('credits');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
        $this->crud->addField([
            'label' => 'Photos',
            'type' => 'dropzone_media',
            'view_namespace' => 'dropzone::fields',
            'name' => 'images',
            'collection' => 'photos',
            'thumb_collection' => 'thumb',
            'options' => [
                'thumbnailHeight' => 120,
                'thumbnailWidth' => 120,
                'maxFilesize' => 30,
                'addRemoveLinks' => true,
                'createImageThumbnails' => true,
            ],
        ]);
    }

    public function bulkMerge()
    {
        $this->crud->hasAccessOrFail('create');

        $entries = $this->crud->getRequest()->input('entries');
        $clonedEntries = [];

        $newAlbum = $this->crud->model->find($entries[0])->replicate();
     
        $newAlbum->push();
        $title = "";
        foreach ($entries as $key => $id) {
            if($entry = $this->crud->model->find($id)){
                $title .= " ".$entry->title;
                $media = $entry->getMedia('photos');
                foreach($media as $m){
                    $m->model_id=$newAlbum->id;
                    $m->save();
                }
                $entry->delete();
            }
        }
        $newAlbum->title = $title;
        $newAlbum->save();
/*
        foreach ($entries as $key => $id) {
            if ($entry = $this->crud->model->find($id)) {
                $clonedEntries[] = $entry->replicate()->push();
            }
        }
*/
        return true;
    }
    protected function setupBulkMergeRoutes($segment, $routeName, $controller)
    {
        Route::post($segment.'/bulk-merge', [
            'as'        => $routeName.'.bulkMerge',
            'uses'      => $controller.'@bulkMerge',
            'operation' => 'bulkMerge',
        ]);
    }
    protected function setupBulkMergeDefaults()
    {
        $this->crud->allowAccess('bulkMerge');

        $this->crud->operation('list', function () {
            $this->crud->enableBulkActions();
            $this->crud->addButton('bottom', 'bulk_merge', 'view', 'kda-album-admin::buttons.bulk_merge', 'beginning');
        });
    }

    protected function setupAnnotateRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/{id}/annotate', [
            'as'        => $routeName.'.getAnnotate',
            'uses'      => $controller.'@getAnnotateForm',
            'operation' => 'annotate',
        ]);
        Route::post($segment.'/{id}/annotate', [
            'as'        => $routeName.'.postAnnotate',
            'uses'      => $controller.'@postAnnotateForm',
            'operation' => 'annotate',
        ]);
    }

    public function getAnnotateForm($id) 
    {
        $this->crud->hasAccessOrFail('update');
        $this->crud->setOperation('annotate');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['media']= $this->data['entry']->getMedia('photos');
        $this->data['crud'] = $this->crud;
        $this->data['title'] = 'Annotate '.$this->crud->entity_name;

        return view('kda-album-admin::form.annotate', $this->data);
    }

    public function postAnnotateForm(Request $request = null)
    {
        $this->crud->hasAccessOrFail('update');

        // TODO: do whatever logic you need here
        // ...
        // You can use 
        // - $this->crud
        // - $this->crud->getEntry($id)
        // - $request
        // ...
        $media = request()->input('media');
        // show a success message
        $annotations = request()->input('annotation');
        foreach($media as $media_id){
            $m = \Spatie\MediaLibrary\MediaCollections\Models\Media::find($media_id);
            $annotation = request()->input('annotation.'.$m->id);
            $home = request()->input('home.'.$m->id);
            if($annotation){
               $m->setCustomProperty('annotation',$annotation);
            }else{
                $m->forgetCustomProperty('annotation');
 
             }
            
            if($home){
               $m->setCustomProperty('home',1);
            }else{
               $m->forgetCustomProperty('home');

            }
            $m->save();
        }

        
        \Alert::success('Annotation saved for this entry.')->flash();

        return \Redirect::to($this->crud->route);
    }
    protected function setupAnnotateDefaults()
    {
      $this->crud->allowAccess('annotate');
      $this->crud->operation(['list'], function() {
        $this->crud->addButton('line', 'annotate', 'view', 'kda-album-admin::buttons.annotate');
      });
    }

    
}
