<?php

namespace KDA\Album\Admin;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasTranslations;
    use \KDA\Laravel\Traits\HasDynamicSidebar;
    use \KDA\Laravel\Traits\HasDumps;
    protected $dumps=[
        'albums'    
    ];
    protected $packageName='kda-album-admin';
    protected $viewNamespace = 'kda-album-admin';
    protected $publishViewsTo = 'vendor/kda/backpack/album';
    protected $sidebars = [
        [
            'label'=>'Album',
            'route'=> 'album',
            'behavior'=>'disable',
            'icon'=>'la-route'
        ],
       
    ];
    protected $routes = [
        'backpack/album.php'
    ];
    protected $configs = [
        'kda/album-admin.php'=> 'kda.album-admin'
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}
