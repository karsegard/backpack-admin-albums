@extends(backpack_view('layouts.top_left'))


@section('header')
    <section class="container-fluid">
        <h2>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small>{!! $crud->getSubheading() ?? 'Moderate ' . $crud->entity_name !!}.</small>

            @if ($crud->hasAccess('list'))
                <small><a href="{{ url($crud->route) }}" class="hidden-print font-sm"><i
                            class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }}
                        <span>{{ $crud->entity_name_plural }}</span></a></small>
            @endif
        </h2>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Annoter</h3>
                </div>
                <div class="card-body  ">
                    <form method="POST" action="{{url($crud->route.'/'.$entry->getKey().'/annotate')}}">
                        @csrf
                        @foreach ($media as $m)
                            <input type="hidden" name="media[{{$m->id}}]" value="{{$m->id}}"/>
                            <div class="pb-3">
                                <div s="2">
                                    <img width="150" src="{{ $m->getUrl('thumb') }}" />
                                </div>
                                <div s="1" class="align-self-center">
                                    <input type="checkbox" name="home[{{ $m->id }}]" class="form-control"
                                        value="1" {{ $m->getCustomProperty('home') ? 'checked': ''}}  />
                                </div>
                                <div s="7" class="align-self-center form-group">
                                    <input type="text" name="annotation[{{ $m->id }}]" class="form-control"
                                        placeholder="copyright, auteur etc"
                                        value="{{ $m->getCustomProperty('annotation') }}" />
                                </div>
                            </div>
                        @endforeach
                        <button type="submit">Sauver</button>
                    </form>
                </div><!-- /.card-body -->
            </div><!-- /.card -->
            </form>
        </div>
    </div>
@endsection
