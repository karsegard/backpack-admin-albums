@if ($crud->hasAccess('annotate'))
    <a href="{{ url($crud->route.'/'.$entry->getKey().'/annotate') }}" class="btn btn-sm btn-link"><i class="fa fa-list"></i> Annoter</a>
@endif