@if ($crud->hasAccess('bulkMerge') && $crud->get('list.bulkActions'))
    <a href="javascript:void(0)" onclick="bulkMergeEntries(this)" class="btn btn-sm btn-secondary bulk-button"><i
            class="fa fa-merge"></i> Fusionner</a>
@endif

@push('after_scripts')
    <script>
        if (typeof bulkMergeEntries != 'function') {
            function bulkMergeEntries(button) {

                if (typeof crud.checkedItems === 'undefined' || crud.checkedItems.length < 2) {
                    new Noty({
                        type: "warning",
                        text: "<strong>Pas assez d'élément sélectionnés</strong>"
                    }).show();

                    return;
                }

                var message = "Are you sure you want to merge these :number entries?";
                message = message.replace(":number", crud.checkedItems.length);

                // show confirm message
                swal({
                    title: "{{ trans('backpack::base.warning') }}",
                    text: message,
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "{{ trans('backpack::crud.cancel') }}",
                            value: null,
                            visible: true,
                            className: "bg-secondary",
                            closeModal: true,
                        },
                        delete: {
                            text: "Merge",
                            value: true,
                            visible: true,
                            className: "bg-primary",
                        }
                    },
                }).then((value) => {
                    if (value) {
                        var ajax_calls = [];
                        var merge_route = "{{ url($crud->route) }}/bulk-merge";

                        // submit an AJAX delete call
                        $.ajax({
                            url: merge_route,
                            type: 'POST',
                            data: {
                                entries: crud.checkedItems
                            },
                            success: function(result) {
                                // Show an alert with the result
                                new Noty({
                                    type: "success",
                                    text: "<strong>Entries merged</strong><br>" + crud
                                        .checkedItems.length + " new entries have been added."
                                }).show();

                                crud.checkedItems = [];
                                crud.table.ajax.reload();
                            },
                            error: function(result) {
                                // Show an alert with the result
                                new Noty({
                                    type: "danger",
                                    text: "<strong>Cloning failed</strong><br>One or more entries could not be created. Please try again."
                                }).show();
                            }
                        });
                    }
                });
            }
        }
    </script>
@endpush
